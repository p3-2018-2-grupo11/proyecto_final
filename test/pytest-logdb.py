import sys

if len(sys.argv) != 3:
    print("Numero incorrecto de parametros.\n");
    print("\tFORMATO: python3 pytest-logdb.py <nombre-db> <num-entradas>\n");
    exit(1)

nombre_bd    = sys.argv[1]
num_entradas = int(sys.argv[2])

archivo_bd = open(nombre_bd, "w")

archivo_bd.write("@INICIO!:@NA\n")
for i in range(1, num_entradas):
    archivo_bd.write("llave{}:valor{}\n".format(i, i))

archivo_bd.close()
    
