all: ./bin/logdb ./bin/cliente

./bin/logdb: ./src/logdb.c ./src/db_server_errors.c ./src/base_de_datos.c
	gcc -Wall -g $^ -Iinclude -o $@

./bin/cliente: ./lib/liblogdb.so ./src/cliente.c ./src/db_server_errors.c ./src/db_client_errors.c ./src/cliente-backend.c
	gcc -Wall -g $^ -Iinclude ./lib/liblogdb.so -o $@

./lib/liblogdb.so: ./src/cliente-backend.c
	gcc -Wall -fPIC -shared -I ./include/ ./src/cliente-backend.c -o $@

.PHONY: clean
clean:
	rm -rf bin/ obj/
	mkdir bin/ obj/
	mkdir bin/db
