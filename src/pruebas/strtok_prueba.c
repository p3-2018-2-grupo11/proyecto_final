#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(){

	const int N = 3;
	const char* inmutable_lines[N] = {
		"one number:value number\n",
		"second number:another value number\n",
		"third num:he he another\n"
	};

	char* mutable_lines[N];
	for (int i = 0; i < N; i++){
		mutable_lines[i] = (char*) malloc(strlen(inmutable_lines[i]));
		strcpy(mutable_lines[i], inmutable_lines[i]);
	}

	for (int i = 0; i < N; i++){
		char* key = strtok(mutable_lines[i], ":");
		char* value = strtok(NULL, "\n");

		printf("%s => %s\n", key, value);	
	}

	return 0;
}


