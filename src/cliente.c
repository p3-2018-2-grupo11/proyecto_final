#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <arpa/inet.h>
#include <sys/socket.h>

#include "logdb.h"
#include "db_client_errors.h"
#include "db_server_errors.h"

#define MAX_SLEEP 64
#define BUFFER_SIZE 2048

static char* _getline(char*, int);

int main(int argc, const char* argv[]){

	if (argc < 3){
		fprintf(stderr, "Uso: %s <IP> <PUERTO>\n", argv[0]);
		exit(EXIT_FAILURE);
	}
	
	const char* raw_ip                = argv[1];
	int raw_port                      = atoi(argv[2]);

	char action_buffer[BUFFER_SIZE];
	memset(action_buffer, 0, BUFFER_SIZE - 1);

	char db_name_buffer[BUFFER_SIZE];
	memset(db_name_buffer, 0, BUFFER_SIZE - 1);

	char key_buffer[BUFFER_SIZE];
	memset(key_buffer, 0, BUFFER_SIZE - 1);

	char value_buffer[BUFFER_SIZE];
	memset(value_buffer, 0, BUFFER_SIZE - 1);

	conexionlogdb* connection = NULL;
	int result = 0;

	fprintf(stderr, "Bienvendio al cliente de `logdb`.\n");

	while (1){
		fprintf(stderr, "\nIngresar accion: ");
		_getline(action_buffer, BUFFER_SIZE);

		if (strcmp(action_buffer, "conectar") == 0){
			fprintf(stderr, "-> Iniciando conexion con servidor.\n");
			connection = conectar_db(raw_ip, raw_port); 
			fprintf(stderr, "-> Conexion inicializada.\n");

		} else if (strcmp(action_buffer, "abrir") == 0){
			fprintf(stderr, "-> Nombre de base de datos existente: ");
			_getline(db_name_buffer, BUFFER_SIZE);
			
			result = abrir_db(connection, db_name_buffer);
			if (result == 0){
				fprintf(stderr, "-> Base de datos [%s] se abrio con exito.\n", db_name_buffer);
			} else {
				const char* client_e_msg = db_client_strerror(db_client_errno);
				const char* server_e_msg = db_server_strerror(db_server_errno);

				fprintf(stderr, "-> No se pudo abrir base de datos [%s].\n"
						"--> Error cliente: %s\n"
						"--> Error Servidor: %s\n", 
						db_name_buffer, client_e_msg, server_e_msg);
			}
		} else if (strcmp(action_buffer, "crear") == 0){
			fprintf(stderr, "-> Nombre de la nueva base de datos: ");
			_getline(db_name_buffer, BUFFER_SIZE);
			
			result = crear_db(connection, db_name_buffer);
			if (result == 0){
				fprintf(stderr, "-> Base de datos [%s] se creo y abrio con exito.\n", db_name_buffer);
			} else {
				const char* client_e_msg = db_client_strerror(db_client_errno);
				const char* server_e_msg = db_server_strerror(db_server_errno);

				fprintf(stderr, "-> No se pudo crear base de datos [%s].\n"
						"--> Error cliente: %s\n"
						"--> Error Servidor: %s\n", 
						db_name_buffer, client_e_msg, server_e_msg);
			}
		} else if (strcmp(action_buffer, "insertar") == 0){
			fprintf(stderr, "-> Llave: ");
			_getline(key_buffer, BUFFER_SIZE);

			fprintf(stderr, "-> Valor: ");
			_getline(value_buffer, BUFFER_SIZE);

			result = put_val(connection, key_buffer, value_buffer);

			if (result == 0){
				fprintf(stderr, "-> Valor [%s] con llave [%s] fue insertado con exito.\n",
					       	value_buffer, key_buffer); 
			} else {
				const char* client_e_msg = db_client_strerror(db_client_errno);
				const char* server_e_msg = db_server_strerror(db_server_errno);

				fprintf(stderr, "-> No se pudo insertar valor [%s]:[%s].\n"
						"--> Error cliente: %s\n"
						"--> Error Servidor: %s\n", 
						key_buffer, value_buffer, client_e_msg, server_e_msg);
			}
		} else if (strcmp(action_buffer, "obtener") == 0 ){
			fprintf(stderr, "-> Llave: ");
			_getline(key_buffer, BUFFER_SIZE);

			char* obtained_value = get_val(connection, key_buffer);

			if (obtained_value != NULL){
				fprintf(stderr, "-> Valor obtenido: ");
				fflush(stderr);

				printf("%s\n", obtained_value);
				fflush(stdout);
				
				free(obtained_value);
			} else {
				const char* client_e_msg = db_client_strerror(db_client_errno);
				const char* server_e_msg = db_server_strerror(db_server_errno);

				fprintf(stderr, "-> No se pudo obtener valor de llave [%s].\n"
						"--> Error cliente: %s\n"
						"--> Error Servidor: %s\n", 
						key_buffer, client_e_msg, server_e_msg);
			}
		} else if (strcmp(action_buffer, "eliminar") == 0){
				
			fprintf(stderr, "-> Llave: ");
			_getline(key_buffer, BUFFER_SIZE);

			result = eliminar(connection, key_buffer);

			if (result == 0){
				fprintf(stderr, "-> Valor con llave [%s] fue eliminado con exito.\n", key_buffer); 
			} else {
				const char* client_e_msg = db_client_strerror(db_client_errno);
				const char* server_e_msg = db_server_strerror(db_server_errno);

				fprintf(stderr, "-> No se pudo eliminar valor con llave [%s].\n"
						"--> Error Cliente: %s\n"
						"--> Error Servidor: %s\n", 
						key_buffer, client_e_msg, server_e_msg);
			}
		} else if (strcmp(action_buffer, "cerrar") == 0){
			cerrar_db(connection);

			if (db_server_errno == 0){
				fprintf(stderr, "-> La base de datos y conexion fueron cerradas con exito.\n"); 
			} else {
				const char* client_e_msg = db_client_strerror(db_client_errno);
				const char* server_e_msg = db_server_strerror(db_server_errno);

				fprintf(stderr, "-> No se pudo cerrar la base de datos ni la conexion.\n"
						"--> Error Cliente: %s\n"
						"--> Error Servidor: %s\n", 
						client_e_msg, server_e_msg);
			}
			break;
		} else if (strcmp(action_buffer, "ayuda") == 0) {
			fprintf(stderr, "\nComandos:\n"
					"-> ayuda\n" 	// WORKING
					"-> conectar\n" // WORKING
					"-> abrir\n"	// WORKING
					"-> crear\n"	// NOT WORKING
					"-> insertar\n" // WORKING
					"-> obtener\n"  // NOT WORKING
					"-> eliminar\n" // WORKING
					"-> cerrar\n"); // WORKING
		} else {
			fprintf(stderr, "-> No entiendo ese comando (escribe `ayuda` para mas informacion).\n");
		}
	}



	exit(EXIT_SUCCESS);
}

static char* _getline(char* s, int n){
	char* result = fgets(s, n, stdin);
	if (result == NULL) return result;

	size_t s_len = strlen(s);
	s[s_len - 1] = '\0';

	return s;
}
