#ifndef DB_CLIENT_ERRNO_H
#define DB_CLIENT_ERRNO_H

typedef enum Db_Client_Error_ENUM {
	CLIENT_SUCCESS = 0,

	NO_CONNECTION = 1,
	NO_OPEN_DB,
	FAILED_TYPE_SEND,
	FAILED_SIZE_SEND,
	FAILED_ARG_SEND,
	FAILED_RESULT_RECEIVE,

	FAILED_SIZE_RECEIVE,
	FAILED_VALUE_RECEIVE,

	DB_STILL_OPEN,

	SERVER_ERROR
} Db_Client_Error;

extern int db_client_errno;

const char* db_client_strerror(int db_client_errno);

#endif
