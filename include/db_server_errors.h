#ifndef DB_SERVER_ERRNO_H
#define DB_SERVER_ERRNO_H

typedef enum Db_Server_Error_ENUM {
	SERVER_SUCCESS = 0,

	CANNOT_CREATE_FILE = 1,
	DB_ALREADY_LOADED,
	DB_DOES_NOT_EXIST,
	DB_NOT_LOADED,

	KEY_DOES_NOT_EXIST,
	KEY_HAS_NO_VALUE,

	SIZE_MESSAGE_FAILED,
	VALUE_MESSAGE_FAILED,
	DB_DIR_RECEIVE_FAILED,

	NULL_DB_DIR,
	NULL_DB_KEY,
	NULL_DB_VALUE,

	CONNECTION_CLOSE_FAILED
} Db_Server_Error;

extern int db_server_errno;

const char* db_server_strerror(int db_server_errno);




#endif
